document.addEventListener('DOMContentLoaded', function() {
  'use strict';

  SmoothScroll.default.start({forcePolyfill: true});
});

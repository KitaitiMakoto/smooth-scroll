const path = require('path');

module.exports = {
  context: path.resolve(__dirname, './src'),
  entry: {
    "smooth-scroll": './smooth-scroll.js',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    library: 'SmoothScroll'
  }
};

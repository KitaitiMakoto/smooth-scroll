'use strict';

class SmoothScroll {
  constructor(element) {
    this.element = element;
  }

  get duration() {
    return 1000; // milliseconds
  }

  // @todo cache id until href attribute changes
  get target() {
    let href = this.element.getAttribute('href');
    if (!href || !href.startsWith('#')) {
      return null;
    }
    let id = href.slice(1);
    return document.getElementById(id);
  }

  enable() {
    this.clickListener = event => this.onclick(event);
    this.element.addEventListener('click', this.clickListener);
  }

  disable() {
    this.element.removeEventListener('click', this.clickListener);
  }

  onclick(event) {
    let oldURL = location.href;
    if (this.scrollToTarget({oldURL})) {
      event.preventDefault();
      this.pushHistoryState();
      this.dispatchHashChangeEvent(oldURL);
    }
  }

  scrollToTarget({oldURL}) {
    if (! this.target) {
      return;
    }
    var startPosition = window.pageYOffset;
    var distance = this.getScrollingDistance(startPosition);
    if (distance === 0) {
      return;
    }

    var duration = this.duration;
    var startTime = performance.now();
    var endTime = startTime + duration;

    var getCurrentOffset = this.getCurrentOffset;
    var ease = this.ease;

    return requestAnimationFrame(move);

    function move(timestamp) {
      var currentPosition = startPosition + getCurrentOffset(distance, startTime, duration, ease);
      scrollTo(window.pageXOffset, currentPosition);
      if (performance.now() >= endTime) {
        return;
      }
      requestAnimationFrame(move);
    }
  }

  getScrollingDistance(startPosition) {
    var targetPosition = this.target.offsetTop;
    var distance = targetPosition - startPosition;

    var pageBottomPosition = document.body.scrollHeight;
    var targetTopPositionFromPageTop = window.pageYOffset + this.target.getBoundingClientRect().top;
    var elementTopFromPageBottom = pageBottomPosition - targetTopPositionFromPageTop;

    var distanceToElementTopFromPageBottom = elementTopFromPageBottom - startPosition;
    if(distance > distanceToElementTopFromPageBottom) {
      targetPosition = pageBottomPosition - window.innerHeight;
      distance = targetPosition - startPosition;
    }
    return distance;
  }

  getCurrentOffset(distance, startTime, duration, ease) {
    var now = performance.now();
    var ratio = (now - startTime) / duration;
    ratio = Math.min(ratio, 1);
    return distance * ease(ratio);
  }

  ease(t) {
    return 1 - Math.pow(2, -10 * t);
  }

  pushHistoryState() {
    let title = this.element.title || this.element.textContent;
    history.pushState(null, title, this.element.href);
  }

  dispatchHashChangeEvent(oldURL) {
    let newURL = this.element.href;
    if (oldURL === newURL) {
      return;
    }
    let event = new HashChangeEvent('hashchange', {
      bubbles: true,
      oldURL: oldURL,
      newURL: newURL
    });
    dispatchEvent(event);
  }

  static start(options = {}) {
    options = Object.assign({}, this.OPTIONS, options);
    if (!options.forcePolyfill && CSS.supports('scroll-behavior', 'smooth')) {
      return;
    }

    let links = document.querySelectorAll('a[href^="#"]');
    for (let link of links) {
      let wrapper = new this(link);
      wrapper.enable();
    }
  }
}

SmoothScroll.OPTIONS = {
  forcePolifyll: false
};

export default SmoothScroll;

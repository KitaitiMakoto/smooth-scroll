Smooth Scroll
=============

History-state-aware smooth scroll. It make it possible to restore scrolling position when back button is pressed.

DEMO
----

https://kitaitimakoto.gitlab.io/smooth-scroll/

USAGE
-----

```javascript
import SmoothScroll from 'smooth-scroll';

document.addEventListener('DOMContentLoaded', () => SmoothScroll.start());
```

TODO
----

* Make changeable easing function
* Consider scrolling box other than `body` element
* target class
* scrolling class
* scrollstart event
* scrollend event
* option to specify elememts
